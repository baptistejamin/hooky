/*
 * Hooky
 *
 * Copyright 2015, Baptiste Jamin
 * Author: Baptiste Jamin <baptiste@jamin.me>
 */

var express = require('express');
var bodyParser = require('body-parser');
var config = require('./config');
var bitbucket = require('./libs/bitbucket');

var app = express();
var bodyparser = bodyParser.urlencoded();

app.post('/deploy/:key', bodyparser, function(req, res){

    if(req.params.key && req.params.key == config.key){
        bitbucket.handleCommit(req);
        res.send(200, "OK");
    }
    else{
        res.send(403, "ERROR");
    }

    return;
});

app.listen(4242, function () {
    console.log('Server ready. Waiting for a commit hook');
});