var config = {
	deployCommand: "deploy",
	key: "abc",
	scripts:{
		"https://bitbucket.org/USERNAME/REPOSITORY1/":[
			"ls",
			"ps"
		],
		"https://bitbucket.org/USERNAME/REPOSITORY2/":[
			"ls",
			"ps -ef"
		]
	}
};

module.exports = config;