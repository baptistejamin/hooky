var deploy = require("./deploy");

var handleCommit = function(req){
  if(typeof req.body.payload === undefined){
  	return;
  }

  var payload = JSON.parse(req.body.payload);

  bburl = payload.canon_url + payload.repository.absolute_url;

  if(!deploy.checkRepository(bburl)){
    console.log("repository configuration not found");
  	return;
  }

   for(var i = 0; i < payload.commits.length; i++) {
        if(payload.commits[i].message.indexOf(config.deployCommand) !=-1){
        	deploy.handle(bburl);
        	return;
        }
   }

   return;
};

module.exports.handleCommit = handleCommit;