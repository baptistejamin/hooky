var config = require("../config");
var sh = require('execSync');

var checkRepository = function(bburl){
	 return (typeof config.scripts[bburl] != "undefined");
};

var handle = function(bburl){
	console.log("Commit received, let's deploy!");

	for(var i = 0; i < config.scripts[bburl].length; i++) {
		console.log(config.scripts[bburl][i]);
        var result = sh.exec(config.scripts[bburl][i]);
		console.log(result.stdout);
   }

   console.log("Deploy done");
};



module.exports.checkRepository = checkRepository;
module.exports.handle = handle;