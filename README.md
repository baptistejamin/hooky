Hooky: hook handler, to make automatic deployments
===================

Hooky is made to run tasks with a POST request.

It is usefull for automatic deployments, from, BitBucket, etc...

## Setup

You must configure the config.js . Setup here your key (default is abc) and your repositories and commands assicated for automatic deployments.

The hook handler adress is http://YOUR_SERVER:4242/deploy/YOUR_KEY

Configure this address on your bitbucket repository

## How to deploy

your commit message must be: deploy

## Let's run it!

```
npm install
npm start

```
